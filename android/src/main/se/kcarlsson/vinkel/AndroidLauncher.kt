package se.kcarlsson.vinkel

import android.os.Bundle
import com.badlogic.gdx.Application
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import se.kcarlsson.vinkel.app.ScreenManager

class AndroidLauncher : AndroidApplication() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        logLevel = Application.LOG_DEBUG

        val coordTransformApi = AndroidCoordTransformApi()
        val stringApi = AndroidStringsApi(this)
        val dialogApi = AndroidDialogApi(this)
        val screenManager = ScreenManager(coordTransformApi, stringApi, dialogApi)

        val config = AndroidApplicationConfiguration()
        config.useRotationVectorSensor = true
        config.useAccelerometer = true
        config.useCompass = true

        initialize(screenManager, config)
    }
}
