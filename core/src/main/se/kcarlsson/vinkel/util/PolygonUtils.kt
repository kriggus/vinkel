package se.kcarlsson.vinkel.util

import com.badlogic.gdx.math.MathUtils.PI2
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Vector2
import kotlin.math.sign

fun Polygon.circle(originX: Float, originY: Float, radius: Float): Polygon {
    if (vertices.size < 8) throw IllegalArgumentException("Circle must contain at least 4 vertices.")
    if (vertices.size % 2 != 0) throw IllegalArgumentException("Circle must have an even number of components.")

    val noOfVertices = vertices.size / 2
    vertices[0] = originX - radius
    vertices[1] = originY
    val sectionRad = PI2 / noOfVertices
    for (i in 2..vertices.lastIndex step 2) {
        val x = vertices[i - 2]
        val y = vertices[i - 1]
        vertices[i] = rotateClockwiseAroundX(x, y, sectionRad, originX, originY)
        vertices[i + 1] = rotateClockwiseAroundY(x, y, sectionRad, originX, originY)
    }
    dirty()
    return this
}

fun Polygon.rect(originX: Float, originY: Float, width: Float, height: Float): Polygon {
    if (vertices.size != 8) throw IllegalArgumentException("Rectangle must contain exactly 4 vertices.")

    vertices[0] = originX - width / 2f
    vertices[1] = originY - height / 2f
    vertices[2] = originX - width / 2f
    vertices[3] = originY + height / 2f
    vertices[4] = originX + width / 2f
    vertices[5] = originY + height / 2f
    vertices[6] = originX + width / 2f
    vertices[7] = originY - height / 2f
    dirty()
    return this
}

fun Polygon.rotateClockwiseAround(originX: Float, originY: Float, radians: Float): Polygon {
    for (i in vertices.indices step 2) {
        val x = vertices[i]
        val y = vertices[i + 1]
        vertices[i] = rotateClockwiseAroundX(x, y, radians, originX, originY)
        vertices[i + 1] = rotateClockwiseAroundY(x, y, radians, originX, originY)
    }
    dirty()
    return this
}

fun FloatArray.clear(): FloatArray {
    for (i in indices) {
        this[i] = 0f
    }
    return this
}

fun FloatArray.setFrom(index: Int, vertices: GdxArray<Vector2>): FloatArray {
    if (index < 0) throw IndexOutOfBoundsException("Index cannot be negative.")
    if (index + vertices.size * 2 - 1 > this.lastIndex) throw IndexOutOfBoundsException("Number of vertices from index exceed polygon size.")
    if (vertices.size % 2 != 0) throw IllegalArgumentException("Vertices to add must have an even number of components.")

    for (i in 0 until vertices.size) {
        this[index + i * 2] = vertices[i].x
        this[index + i * 2 + 1] = vertices[i].y
    }
    return this
}

fun FloatArray.setAt(index: Int, vertex: Vector2): FloatArray {
    if (index < 0) throw IndexOutOfBoundsException("Index cannot be negative.")
    if (index + 1 > lastIndex) throw IndexOutOfBoundsException("Vertex from index exceed polygon size.")

    this[index] = vertex.x
    this[index + 1] = vertex.y
    return this
}

fun FloatArray.reverseVertices() {
    reverse()
    for (i in 0 until size step 2) {
        swap(i, i + 1)
    }
}

private fun FloatArray.swap(index1: Int, index2: Int) {
    val tmp = this[index1]
    this[index1] = this[index2]
    this[index2] = tmp
}

// Only for convex polygons
// See http://www.faqs.org/faqs/graphics/algorithms-faq/ and https://en.wikipedia.org/wiki/Curve_orientation
fun FloatArray.isClockwise(): Boolean {
    val x1 = get(0)
    val y1 = get(1)
    val x2 = get(2)
    val y2 = get(3)
    val x3 = get(4)
    val y3 = get(5)
    val xPrev = x1 - x2
    val yPrev = y1 - y2
    val xNext = x3 - x2
    val yNext = y3 - y2
    return sign(xPrev * yNext - yPrev * xNext) == 1f
}
