package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Vector2
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.model.domain.Orientation
import se.kcarlsson.vinkel.util.GdxArray
import se.kcarlsson.vinkel.util.circle
import se.kcarlsson.vinkel.view.render.ScreenSize
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sign

class CirclesState(
    private val screen: ScreenSize = AppScreenSize.instance
) {
    private val targetOffset = Vector2()
    private val radius = screen.min / 6f

    val offset = Vector2()
    val positions = GdxArray<Vector2>(arrayOf(Vector2(), Vector2()))

    val circle1 = Polygon(FloatArray(128))
    val circle2 = Polygon(FloatArray(128))

    var isVisible = true
        private set

    fun update(pitch: Float, roll: Float, orientation: Orientation) {
        updateTargetOffset(pitch, roll)
        interpolateToTargetOffset()
        updateCircles()
        isVisible = orientation == Orientation.FLAT
    }

    private fun updateTargetOffset(pitch: Float, roll: Float) {
        targetOffset.x = sign(roll) * circleTargetOffsetProportion(roll) * maxCircleTargetOffset()
        targetOffset.y = -sign(pitch) * circleTargetOffsetProportion(pitch) * maxCircleTargetOffset()
    }

    private fun circleTargetOffsetProportion(angle: Float): Float {
        val proportion = abs(angle) / (MathUtils.PI / 4f)
        return .4f * proportion + .6f * proportion.pow(2f)
    }

    private fun maxCircleTargetOffset(): Float =
        screen.max / 2f + radius + screen.max / 10f

    private fun interpolateToTargetOffset() {
        offset.x = MathUtils.lerp(offset.x, targetOffset.x, .33f)
        offset.y = MathUtils.lerp(offset.y, targetOffset.y, .33f)
    }

    private fun updateCircles() {
        positions[0].x = screen.center.x + offset.x
        positions[0].y = screen.center.y + offset.y
        positions[1].x = screen.center.x - offset.x
        positions[1].y = screen.center.y - offset.y
        circle1.circle(positions[0].x, positions[0].y, radius)
        circle2.circle(positions[1].x, positions[1].y, radius)
    }
}
