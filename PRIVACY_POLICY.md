# Privacy Policy - Vinkel
#### Last updated: May 1, 2022

This document is the Privacy Policy of the Android application **"Vinkel"** (later referred as the **"App"**) created by Kristofer Carlsson.

The App is intended to measure the mobile device’s orientation in space relative to the ground using the onboard physical sensors. All measurements and angles are approximations.

- The App does not require any user registration.
- The App does not require the user to provide any personal information.
- The App does not store any personal information locally on the device.
- The App does not collect any information automatically.
- The App does not collect any information about the location of your mobile device.
- The App does not share any information with third parties.
- The App does not address anyone under the age of 13.

This Privacy Policy may be updated at any time for any reason. The updated Privacy Policy will be posted here at https://bitbucket.org/kriggus/vinkel/src/master/PRIVACY_POLICY.md with an updated "Last updated" date.

If you have any questions regarding privacy or data usage please contact via email at: kristofer [at] kcarlsson [dot] se.
