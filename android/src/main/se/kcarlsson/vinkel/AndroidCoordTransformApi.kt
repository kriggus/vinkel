package se.kcarlsson.vinkel

import android.hardware.SensorManager
import android.hardware.SensorManager.AXIS_MINUS_X
import android.hardware.SensorManager.AXIS_Y
import se.kcarlsson.vinkel.app.CoordTransformApi

class AndroidCoordTransformApi : CoordTransformApi {
    override fun remapMatrixToYXZ(rotation: FloatArray, result: FloatArray): FloatArray {
        SensorManager.remapCoordinateSystem(rotation, AXIS_Y, AXIS_MINUS_X, result)
        return result
    }
}