package se.kcarlsson.vinkel

import android.content.Context
import se.kcarlsson.vinkel.app.StringsApi

class AndroidStringsApi(
    private val context: Context
) : StringsApi {

    override val appName: String
        get() = context.getString(R.string.app_name)

    override val usageBtnText: String
        get() = context.getString(R.string.usage_btn_text)

    override val usageDlgTitle: String
        get() = "${context.getString(R.string.usage_btn_text)} - ${context.getString(R.string.app_name)} ${BuildConfig.VERSION_NAME}"

    override val usageDlgMessage: String
        get() = context.getString(R.string.usage_dlg_message)
}
