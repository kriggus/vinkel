package se.kcarlsson.vinkel.util

import com.badlogic.gdx.utils.Array

typealias GdxArray<T> = Array<T>