package se.kcarlsson.vinkel.model.domain

import se.kcarlsson.vinkel.app.CoordTransformApi

class CopyCoordTransformFake : CoordTransformApi {
    override fun remapMatrixToYXZ(rotation: FloatArray, result: FloatArray): FloatArray {
        return rotation
    }
}
