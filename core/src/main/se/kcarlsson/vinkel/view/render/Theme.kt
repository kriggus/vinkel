package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.Color

interface Theme {
    val background: Color
    val foreground: Color

    val debugForegroundOne: Color
    val debugForegroundTwo: Color
    val debugForegroundThree: Color
}