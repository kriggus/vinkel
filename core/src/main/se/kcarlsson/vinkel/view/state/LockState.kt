package se.kcarlsson.vinkel.view.state

import se.kcarlsson.vinkel.model.domain.Orientation
import se.kcarlsson.vinkel.model.domain.Orientation.FLAT
import se.kcarlsson.vinkel.view.state.LockMode.DEFAULT
import se.kcarlsson.vinkel.view.state.LockMode.LOCKED

class LockState {

    private var orientation = FLAT

    var previousMode = DEFAULT
        private set

    var mode = DEFAULT
        set(value) {
            previousMode = field
            field = value
        }

    fun update(orientation: Orientation) {
        val isFlat = orientation == FLAT
        val wasFlat = this.orientation == FLAT
        val didFlatChange = (isFlat && !wasFlat) || (!isFlat && wasFlat)
        if (mode == LOCKED && didFlatChange) {
            mode = DEFAULT
        }
        this.orientation = orientation
    }

    fun reset() {
        mode = DEFAULT
        previousMode = DEFAULT
    }
}
