package se.kcarlsson.vinkel.view.state

import se.kcarlsson.vinkel.util.GdxArray

class IntersectionsState {
    private val circleToCircle = Intersection()
    private val circle1ToRectile = GdxArray<Intersection>()
    private val circle2ToRectile = GdxArray<Intersection>()
    private val skyToRectile = GdxArray<Intersection>()

    val intersections = GdxArray<Intersection>()
    val doubleIntersections = GdxArray<Intersection>()

    fun update(circles: CirclesState, sky: SkyState, rectile: ReticleState) {
        reset()
        when {
            circles.isVisible -> updateFromCircles(circles, rectile)
            sky.isVisible -> updateFromSky(sky, rectile)
        }
        refresh()
    }

    private fun updateFromCircles(circles: CirclesState, rectile: ReticleState) {
        circleToCircle.updateFromIntersection(circles.circle1, circles.circle2)
        circle1ToRectile.updateFromIntersection(circles.circle1, rectile.rectile)
        circle2ToRectile.updateFromIntersection(circles.circle2, rectile.rectile)
        if (circleToCircle.isIntersecting) {
            doubleIntersections.updateFromIntersection(circleToCircle.polygon, rectile.inner.rectile)
        }
    }

    private fun updateFromSky(sky: SkyState, rectile: ReticleState) {
        skyToRectile.updateFromIntersection(sky.sky, rectile.rectile)
    }

    private fun reset() {
        intersections.reset()
        doubleIntersections.reset()
    }

    private fun refresh() {
        intersections.clear()
        intersections.add(circleToCircle)
        intersections.addAll(circle1ToRectile)
        intersections.addAll(circle2ToRectile)
        intersections.addAll(skyToRectile)
    }
}
