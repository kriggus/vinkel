package se.kcarlsson.vinkel.model.state

import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement
import se.kcarlsson.vinkel.model.domain.Orientation

class OrientationReader : GyroscopeListener, OrientationHolder {

    override var orientation: Orientation = Orientation.FLAT
        private set

    override fun onMeasure(measurement: Measurement) {
        orientation = measurement.orientation
    }
}