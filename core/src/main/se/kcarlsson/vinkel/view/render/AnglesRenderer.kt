package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.Align
import se.kcarlsson.vinkel.app.AppTheme
import se.kcarlsson.vinkel.view.state.AnglesState

class AnglesRenderer(
    private val shapeRenderer: ShapeRenderer,
    private val spriteBatch: SpriteBatch,
    private val theme: Theme = AppTheme.instance
) {

    fun render(state: AnglesState) {
        renderOverlay(state)
        renderText(state)
    }

    private fun renderOverlay(state: AnglesState) {
        shapeRenderer.color = theme.background
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.rect(state.overlayFrom, state.overlayTo)
        shapeRenderer.circle(state.overlayFromCenter, state.overlayHeight / 2f)
        shapeRenderer.circle(state.overlayToCenter, state.overlayHeight / 2f)
        shapeRenderer.end()
    }

    private fun renderText(state: AnglesState) {
        spriteBatch.begin()
        spriteBatch.transformMatrix = state.textTransform
        state.font.draw(
            spriteBatch,
            state.text,
            0f,
            0f,
            state.overlayWidth,
            Align.center,
            false
        )
        spriteBatch.end()
    }
}
