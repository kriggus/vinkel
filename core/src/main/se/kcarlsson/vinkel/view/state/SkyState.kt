package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Vector2
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.model.domain.Orientation
import se.kcarlsson.vinkel.util.*
import se.kcarlsson.vinkel.view.render.ScreenSize
import kotlin.math.abs

class SkyState(
    private val screen: ScreenSize = AppScreenSize.instance
) {
    private val from = Vector2()
    private val fromTarget = Vector2()
    private val to = Vector2()
    private val toTarget = Vector2()
    private val up = Vector2(0f, 1f)
    private val upTarget = Vector2(0f, 1f)

    private val screenEdgeIntersectionCache = Vector2()

    private val screenCornersAboveHorizon = GdxArray<Vector2>()
    private val threeVerts = FloatArray(6)
    private val fourVerts = FloatArray(8)
    private val fiveVerts = FloatArray(10)

    var hasScreenEdgeIntersections = false
    val screenEdgeIntersections = GdxArray<Vector2>(2).apply {
        add(Vector2())
        add(Vector2())
    }

    val sky = Polygon(fourVerts)

    var isVisible = false
        private set

    fun update(pitch: Float, roll: Float, orientation: Orientation) {
        updateTargets(pitch, roll)
        interpolateToTargets()
        updateScreenEdgeIntersections()
        updatePolygon(roll)
        isVisible = orientation == Orientation.PORTRAIT || orientation == Orientation.LANDSCAPE
    }

    private fun updateTargets(pitch: Float, roll: Float) {
        val rotation = rotation(pitch, roll)
        val offset = offset(roll)

        fromTarget.x = screen.center.x + offset
        fromTarget.y = screen.center.y - signPosZero(roll)
        fromTarget.rotateAroundRad(screen.center, rotation)

        toTarget.x = screen.center.x + offset
        toTarget.y = screen.center.y + signPosZero(roll)
        toTarget.rotateAroundRad(screen.center, rotation)

        upTarget.x = -signPosZero(roll)
        upTarget.y = 0f
        upTarget.rotateRad(rotation)
    }

    private fun rotation(pitch: Float, roll: Float): Float =
        -signPosZero(roll) * pitch

    private fun offset(roll: Float): Float =
        offsetSign(roll) * offsetProportion(roll) * offsetMax()

    private fun offsetSign(roll: Float): Float =
        signPosZero(roll) * signPosZero(abs(roll) - (MathUtils.PI / 2f))

    private fun offsetProportion(roll: Float): Float =
        abs(abs(roll) - MathUtils.PI / 2f) / (MathUtils.PI / 4f)

    private fun offsetMax(): Float =
        screen.max / 2f + screen.max / 10f

    private fun interpolateToTargets() {
        from.x = MathUtils.lerp(from.x, fromTarget.x, .33f)
        from.y = MathUtils.lerp(from.y, fromTarget.y, .33f)

        to.x = MathUtils.lerp(to.x, toTarget.x, .33f)
        to.y = MathUtils.lerp(to.y, toTarget.y, .33f)

        up.x = MathUtils.lerp(up.x, upTarget.x, .33f)
        up.y = MathUtils.lerp(up.y, upTarget.y, .33f)
    }

    private fun updateScreenEdgeIntersections() {
        var index = 0
        index = updateScreenEdgeIntersectionAtIndexIfBetween(index, screen.bottomLeft, screen.topLeft)
        index = updateScreenEdgeIntersectionAtIndexIfBetween(index, screen.topRight, screen.bottomRight)
        index = updateScreenEdgeIntersectionAtIndexIfBetween(index, screen.bottomLeft, screen.bottomRight)
        index = updateScreenEdgeIntersectionAtIndexIfBetween(index, screen.topLeft, screen.topRight)
        hasScreenEdgeIntersections = index == 2
    }

    private fun updateScreenEdgeIntersectionAtIndexIfBetween(index: Int, p1: Vector2, p2: Vector2): Int {
        if (index < 2 && findEdgeIntersectionBetween(p1, p2)) {
            screenEdgeIntersections[index].x = screenEdgeIntersectionCache.x
            screenEdgeIntersections[index].y = screenEdgeIntersectionCache.y
            return index + 1
        }
        return index
    }

    private fun findEdgeIntersectionBetween(p1: Vector2, p2: Vector2): Boolean =
        Intersector.intersectLines(p1, p2, from, to, screenEdgeIntersectionCache) && screen.isOnScreen(screenEdgeIntersectionCache)

    private fun updatePolygon(roll: Float) {
        when {
            hasScreenEdgeIntersections ->
                updatePolygonFromScreenCornersAndEdgeIntersections()
            abs(roll) > MathUtils.PI / 2f ->
                updatePolygonFromScreenCorners()
            else ->
                clearPolygon()
        }
    }

    private fun updatePolygonFromScreenCornersAndEdgeIntersections() {
        updateScreenCornersAboveHorizon()
        val vertices = selectVertices()
        updateFromScreenEdgeIntersections(vertices)
        updateFromScreenCorners(vertices)
        orderClockwise(vertices)
        sky.vertices = vertices
    }

    private fun updateScreenCornersAboveHorizon() {
        for (corner in screen.corners) {
            if (isAboveHorizon(corner)) {
                screenCornersAboveHorizon.add(corner)
            }
        }
    }

    private fun isAboveHorizon(p: Vector2): Boolean {
        val signX = signPosZero(up.x)
        val signY = signPosZero(up.y)
        val isAboveFirst = signX * p.x >= signX * screenEdgeIntersections[0].x && signY * p.y >= signY * screenEdgeIntersections[0].y
        val isAboveSecond = signX * p.x >= signX * screenEdgeIntersections[1].x && signY * p.y >= signY * screenEdgeIntersections[1].y
        return isAboveFirst || isAboveSecond
    }

    private fun selectVertices(): FloatArray =
        when (screenCornersAboveHorizon.size) {
            1 -> threeVerts
            2 -> fourVerts
            3 -> fiveVerts
            else -> throw IllegalStateException("Unsupported number of screen corners above horizon")
        }

    private fun updateFromScreenEdgeIntersections(vertices: FloatArray) {
        vertices.setFrom(0, screenEdgeIntersections)
    }

    private fun updateFromScreenCorners(vertices: FloatArray) {
        var index = 2 * screenEdgeIntersections.size
        var reference = screenEdgeIntersections.last()
        while (screenCornersAboveHorizon.size > 0) {
            var minDst = Float.MAX_VALUE
            var minDstIndex = -1
            for (i in 0 until screenCornersAboveHorizon.size) {
                val corner = screenCornersAboveHorizon[i]
                val dst = when {
                    reference.x == corner.x || reference.y == corner.y -> reference.dst2(corner)
                    else -> Float.MAX_VALUE
                }
                if (dst < minDst) {
                    minDst = dst
                    minDstIndex = i
                }
            }
            reference = screenCornersAboveHorizon.removeIndex(minDstIndex)
            vertices.setAt(index, reference)
            index += 2
        }
    }

    private fun orderClockwise(vertices: FloatArray) {
        if (!vertices.isClockwise()) {
            vertices.reverseVertices()
        }
    }

    private fun updatePolygonFromScreenCorners() {
        sky.vertices = fourVerts.setFrom(0, screen.corners)
    }

    private fun clearPolygon() {
        sky.vertices = fourVerts.clear()
    }
}
