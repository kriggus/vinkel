package se.kcarlsson.vinkel.app

import com.badlogic.gdx.Input
import se.kcarlsson.vinkel.view.render.ScreenSize
import se.kcarlsson.vinkel.view.state.LockMode.*
import se.kcarlsson.vinkel.view.state.ScreenState

class TouchInputHandler(
    private val input: Input,
    private val state: ScreenState,
    private val strings: StringsApi,
    private val dialog: DialogApi,
    private val screen: ScreenSize = AppScreenSize.instance
) : TouchListener {

    private val accThreshold = 2f / 3f
    private var accDelta = 0f
    private var isInfoPressed = false

    override fun onTouchDown(x: Int, y: Int) {
        when {
            isIntersectingInfo(x, y) ->
                isInfoPressed = true
            else ->
                when (state.lock.mode) {
                    LOCKED ->
                        state.lock.mode = WAITING
                    DEFAULT -> {
                        state.lock.mode = STICKY
                        vibrate()
                    }
                    WAITING, STICKY ->
                        Unit
                }
        }
        accDelta = 0f
    }

    override fun onTouchUp(x: Int, y: Int) {
        when {
            isInfoPressed && isIntersectingInfo(x, y) -> {
                vibrate()
                dialog.show(
                    title = strings.usageDlgTitle,
                    message = strings.usageDlgMessage,
                    rotation = state.info.overlayRotation
                )
            }
            !isInfoPressed ->
                when (state.lock.mode) {
                    STICKY -> {
                        state.lock.mode = LOCKED
                        if (accDelta > accThreshold / 2f) {
                            vibrate()
                        }
                    }
                    WAITING -> {
                        state.lock.mode = DEFAULT
                        vibrate()
                    }
                    DEFAULT, LOCKED ->
                        Unit
                }
        }
        accDelta = 0f
        isInfoPressed = false
    }

    fun update(delta: Float) {
        if (!isInfoPressed) {
            if (state.lock.mode == WAITING || state.lock.mode == STICKY) {
                accDelta += delta
            }
            if (state.lock.mode == WAITING && accDelta > accThreshold) {
                state.lock.mode = STICKY
                vibrate()
            }
        }
    }

    fun reset() {
        accDelta = 0f
        isInfoPressed = false
    }

    private fun vibrate() {
        input.vibrate(Input.VibrationType.MEDIUM)
    }

    // Note: Input processor's y values are on inverted y-axis from top left screen corner
    private fun isIntersectingInfo(x: Int, y: Int): Boolean =
        state.info.isIntersecting(x.toFloat(), screen.height - y)
}
