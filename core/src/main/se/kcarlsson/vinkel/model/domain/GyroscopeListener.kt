package se.kcarlsson.vinkel.model.domain

interface GyroscopeListener {
    fun onMeasure(measurement: Measurement)
}
