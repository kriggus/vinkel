package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import se.kcarlsson.vinkel.app.AppTheme
import se.kcarlsson.vinkel.view.state.IntersectionsState

class IntersectionRenderer(
    private val shapeRenderer: ShapeRenderer,
    private val theme: Theme = AppTheme.instance
) {
    fun render(state: IntersectionsState) {
        renderIntersections(state)
        renderDoubleIntersections(state)
    }

    private fun renderIntersections(state: IntersectionsState) {
        shapeRenderer.color = theme.background
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        for (i in 0 until state.intersections.size) {
            if (state.intersections[i].isIntersecting) {
                shapeRenderer.polygonFilled(state.intersections[i].polygon.vertices)
            }
        }
        shapeRenderer.end()
    }

    private fun renderDoubleIntersections(state: IntersectionsState) {
        shapeRenderer.color = theme.foreground
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        for (i in 0 until state.doubleIntersections.size) {
            if (state.doubleIntersections[i].isIntersecting) {
                shapeRenderer.polygonFilled(state.doubleIntersections[i].polygon.vertices)
            }
        }
        shapeRenderer.end()
    }
}
