package se.kcarlsson.vinkel.app

import com.badlogic.gdx.graphics.Color
import se.kcarlsson.vinkel.view.render.Theme

class AppTheme : Theme {
    override val background = Color(.2f, .4f, .7f, 1f)
    override val foreground = Color(1f, .9f, .6f, 1f)

    override val debugForegroundOne = Color(0f, 1f, 0f, 1f)
    override val debugForegroundTwo = Color(0f, 0f, 1f, 1f)
    override val debugForegroundThree = Color(1f, 0f, 1f, 1f)

    companion object {
        val instance: Theme = AppTheme()
    }
}
