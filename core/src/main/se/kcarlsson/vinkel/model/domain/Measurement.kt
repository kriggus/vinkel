package se.kcarlsson.vinkel.model.domain

import se.kcarlsson.vinkel.util.formatWithSign

data class Measurement(
    val orientation: Orientation,
    val anglesXYZ: Angles,
    val anglesYXZ: Angles
) {
    override fun toString(): String =
        "${orientation.value} " +
            ": ${formatWithSign(anglesXYZ.pitch)} ${formatWithSign(anglesXYZ.roll)}" +
            ": ${formatWithSign(anglesYXZ.pitch)} ${formatWithSign(anglesYXZ.roll)}"
}
