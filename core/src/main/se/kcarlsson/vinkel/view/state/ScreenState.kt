package se.kcarlsson.vinkel.view.state

import se.kcarlsson.vinkel.app.StringsApi
import se.kcarlsson.vinkel.model.domain.Orientation.*
import se.kcarlsson.vinkel.model.state.AnglesHolder
import se.kcarlsson.vinkel.model.state.OrientationHolder

class ScreenState(
    strings: StringsApi,
    val lock: LockState = LockState(),
    val circles: CirclesState = CirclesState(),
    val sky: SkyState = SkyState(),
    val reticle: ReticleState = ReticleState(),
    val intersections: IntersectionsState = IntersectionsState(),
    val angles: AnglesState = AnglesState(),
    val info: InfoState = InfoState(strings)
) {
    fun update(flatAngles: AnglesHolder, vertAngles: AnglesHolder, orientation: OrientationHolder) {
        lock.update(orientation.orientation)
        circles.update(flatAngles.pitch, flatAngles.roll, orientation.orientation)
        sky.update(vertAngles.pitch, vertAngles.roll, orientation.orientation)
        reticle.update(lock.mode, circles, sky)
        intersections.update(circles, sky, reticle)
        when (orientation.orientation) {
            FLAT -> {
                angles.updateFlat(flatAngles.pitch, flatAngles.roll)
            }
            LANDSCAPE, PORTRAIT -> {
                angles.updateVert(vertAngles.pitch, vertAngles.roll)
                info.update(vertAngles.pitch, vertAngles.roll)
            }
        }
        angles.updateLock(lock.mode)
    }

    fun dispose() {
        angles.dispose()
    }
}
