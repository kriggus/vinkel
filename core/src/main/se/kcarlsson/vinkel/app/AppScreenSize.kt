package se.kcarlsson.vinkel.app

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import se.kcarlsson.vinkel.util.GdxArray
import se.kcarlsson.vinkel.view.render.ScreenSize
import kotlin.math.max
import kotlin.math.min

class AppScreenSize : ScreenSize {
    override val width: Float = Gdx.graphics.width.toFloat()
    override val height: Float = Gdx.graphics.height.toFloat()
    override val max = max(width, height)
    override val min = min(width, height)
    override val minMaxDiff = max - min

    override val bottomLeft = Vector2(0f, 0f)
    override val topLeft = Vector2(0f, height)
    override val topRight = Vector2(width, height)
    override val bottomRight = Vector2(width, 0f)
    override val center = Vector2(width / 2f, height / 2f)
    override val corners = GdxArray<Vector2>().apply {
        add(bottomLeft)
        add(topLeft)
        add(topRight)
        add(bottomRight)
    }

    companion object {
        val instance: ScreenSize = AppScreenSize()
    }
}
