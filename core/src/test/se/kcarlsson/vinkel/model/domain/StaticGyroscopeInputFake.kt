package se.kcarlsson.vinkel.model.domain

import com.badlogic.gdx.Input
import com.badlogic.gdx.InputProcessor

class StaticGyroscopeInputFake(
    val rotationMatrix: FloatArray = FloatArray(16) { 0f }
) : Input {

    override fun vibrate(milliseconds: Int, fallback: Boolean) {
        throw Exception("Not implemented")
    }

    override fun vibrate(milliseconds: Int, amplitude: Int, fallback: Boolean) {
        throw Exception("Not implemented")
    }

    override fun vibrate(vibrationType: Input.VibrationType?) {
        throw Exception("Not implemented")
    }

    override fun getAccelerometerX(): Float {
        throw Exception("Not implemented")
    }

    override fun getAccelerometerY(): Float {
        throw Exception("Not implemented")
    }

    override fun getAccelerometerZ(): Float {
        throw Exception("Not implemented")
    }

    override fun getGyroscopeX(): Float {
        throw Exception("Not implemented")
    }

    override fun getGyroscopeY(): Float {
        throw Exception("Not implemented")
    }

    override fun getGyroscopeZ(): Float {
        throw Exception("Not implemented")
    }

    override fun getMaxPointers(): Int {
        throw Exception("Not implemented")
    }

    override fun getX(): Int {
        throw Exception("Not implemented")
    }

    override fun getX(pointer: Int): Int {
        throw Exception("Not implemented")
    }

    override fun getDeltaX(): Int {
        throw Exception("Not implemented")
    }

    override fun getDeltaX(pointer: Int): Int {
        throw Exception("Not implemented")
    }

    override fun getY(): Int {
        throw Exception("Not implemented")
    }

    override fun getY(pointer: Int): Int {
        throw Exception("Not implemented")
    }

    override fun getDeltaY(): Int {
        throw Exception("Not implemented")
    }

    override fun getDeltaY(pointer: Int): Int {
        throw Exception("Not implemented")
    }

    override fun isTouched(): Boolean {
        throw Exception("Not implemented")
    }

    override fun isTouched(pointer: Int): Boolean {
        throw Exception("Not implemented")
    }

    override fun justTouched(): Boolean {
        throw Exception("Not implemented")
    }

    override fun getPressure(): Float {
        throw Exception("Not implemented")
    }

    override fun getPressure(pointer: Int): Float {
        throw Exception("Not implemented")
    }

    override fun isButtonPressed(button: Int): Boolean {
        throw Exception("Not implemented")
    }

    override fun isButtonJustPressed(button: Int): Boolean {
        throw Exception("Not implemented")
    }

    override fun isKeyPressed(key: Int): Boolean {
        throw Exception("Not implemented")
    }

    override fun isKeyJustPressed(key: Int): Boolean {
        throw Exception("Not implemented")
    }

    override fun getTextInput(listener: Input.TextInputListener?, title: String?, text: String?, hint: String?) {
        throw Exception("Not implemented")
    }

    override fun getTextInput(
        listener: Input.TextInputListener?,
        title: String?,
        text: String?,
        hint: String?,
        type: Input.OnscreenKeyboardType?
    ) {
        throw Exception("Not implemented")
    }

    override fun setOnscreenKeyboardVisible(visible: Boolean) {
        throw Exception("Not implemented")
    }

    override fun setOnscreenKeyboardVisible(visible: Boolean, type: Input.OnscreenKeyboardType?) {
        throw Exception("Not implemented")
    }

    override fun vibrate(milliseconds: Int) {
        throw Exception("Not implemented")
    }

    override fun getAzimuth(): Float {
        throw Exception("Not implemented")
    }

    override fun getPitch(): Float {
        throw Exception("Not implemented")
    }

    override fun getRoll(): Float {
        throw Exception("Not implemented")
    }

    override fun getRotationMatrix(matrix: FloatArray?) {
        for (i in rotationMatrix.indices) {
            if (i < (matrix?.size ?: 0)) {
                matrix?.set(i, rotationMatrix[i])
            }
        }
    }

    override fun getCurrentEventTime(): Long {
        throw Exception("Not implemented")
    }

    override fun setCatchBackKey(catchBack: Boolean) {
        throw Exception("Not implemented")
    }

    override fun isCatchBackKey(): Boolean {
        throw Exception("Not implemented")
    }

    override fun setCatchMenuKey(catchMenu: Boolean) {
        throw Exception("Not implemented")
    }

    override fun isCatchMenuKey(): Boolean {
        throw Exception("Not implemented")
    }

    override fun setCatchKey(keycode: Int, catchKey: Boolean) {
        throw Exception("Not implemented")
    }

    override fun isCatchKey(keycode: Int): Boolean {
        throw Exception("Not implemented")
    }

    override fun setInputProcessor(processor: InputProcessor?) {
        throw Exception("Not implemented")
    }

    override fun getInputProcessor(): InputProcessor {
        throw Exception("Not implemented")
    }

    override fun isPeripheralAvailable(peripheral: Input.Peripheral?): Boolean {
        throw Exception("Not implemented")
    }

    override fun getRotation(): Int {
        throw Exception("Not implemented")
    }

    override fun getNativeOrientation(): Input.Orientation {
        throw Exception("Not implemented")
    }

    override fun setCursorCatched(catched: Boolean) {
        throw Exception("Not implemented")
    }

    override fun isCursorCatched(): Boolean {
        throw Exception("Not implemented")
    }

    override fun setCursorPosition(x: Int, y: Int) {
        throw Exception("Not implemented")
    }
}
