package se.kcarlsson.vinkel.app

interface TouchListener {
    fun onTouchDown(x: Int, y: Int)
    fun onTouchUp(x: Int, y: Int)
}