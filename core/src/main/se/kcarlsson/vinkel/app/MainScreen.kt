package se.kcarlsson.vinkel.app

import com.badlogic.gdx.Gdx.input
import se.kcarlsson.vinkel.model.domain.Gyroscope
import se.kcarlsson.vinkel.model.state.CombinedMeanAnglesReader
import se.kcarlsson.vinkel.model.state.MeanAnglesReader
import se.kcarlsson.vinkel.model.state.MeanOrientationReader
import se.kcarlsson.vinkel.view.render.ScreenRenderer
import se.kcarlsson.vinkel.view.state.ScreenState

class MainScreen(
    coordTransform: CoordTransformApi,
    strings: StringsApi,
    private val dialog: DialogApi
) : UpdatableScreenAdapter() {

    private val gyroscope = Gyroscope(1f / 60f, input, coordTransform)

    private val logger = GyroscopeLogger()
    private val flatAnglesReader = MeanAnglesReader(12)
    private val vertAnglesReader = CombinedMeanAnglesReader(12)
    private val orientationReader = MeanOrientationReader(12)

    private val state = ScreenState(strings)

    private val renderer = ScreenRenderer()

    private val inputProcessor = TouchInputProcessor()
    private val inputHandler = TouchInputHandler(input, state, strings, dialog)

    init {
        gyroscope.addListener(logger)
        gyroscope.addListener(flatAnglesReader)
        gyroscope.addListener(vertAnglesReader)
        gyroscope.addListener(orientationReader)
        inputProcessor.addListener(inputHandler)
    }

    override fun update(delta: Float) {
        inputHandler.update(delta)
        gyroscope.update(delta)
        state.update(flatAnglesReader, vertAnglesReader, orientationReader)
    }

    override fun show() {
        input.inputProcessor = inputProcessor
        gyroscope.start()
    }

    override fun resume() {
        input.inputProcessor = inputProcessor
        gyroscope.start()
    }

    override fun pause() {
        input.inputProcessor = null
        inputProcessor.reset()
        gyroscope.stop()
        inputHandler.reset()
        state.lock.reset()
        dialog.hide()
    }

    override fun render() {
        renderer.render(state)
    }

    override fun dispose() {
        gyroscope.removeListener(logger)
        gyroscope.removeListener(flatAnglesReader)
        gyroscope.removeListener(vertAnglesReader)
        gyroscope.removeListener(orientationReader)
        inputProcessor.removeListener(inputHandler)
        state.dispose()
        renderer.dispose()
    }
}
