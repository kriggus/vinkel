package se.kcarlsson.vinkel.app

import com.badlogic.gdx.Game

class ScreenManager(
    private val coordTransform: CoordTransformApi,
    private val strings: StringsApi,
    private val dialog: DialogApi
) : Game() {

    override fun create() {
        val screen = MainScreen(coordTransform, strings, dialog)
        setScreen(screen)
    }
}
