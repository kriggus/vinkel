package se.kcarlsson.vinkel.app

interface CoordTransformApi {
    fun remapMatrixToYXZ(rotation: FloatArray, result: FloatArray): FloatArray
}