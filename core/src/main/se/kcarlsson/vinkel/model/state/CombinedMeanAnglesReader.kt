package se.kcarlsson.vinkel.model.state

import com.badlogic.gdx.math.MathUtils
import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement
import se.kcarlsson.vinkel.util.GdxArray
import se.kcarlsson.vinkel.util.diffCyclic
import se.kcarlsson.vinkel.util.norCyclic
import kotlin.math.abs

class CombinedMeanAnglesReader(
    private val meanSampleSize: Int
) : GyroscopeListener, AnglesHolder {

    private val measurements = GdxArray<Measurement>()
    private var meanPitch = 0f
    private var meanRoll = 0f
    private var meanPitchYXZ = 0f
    private var meanRollYXZ = 0f

    override var pitch = 0f
        private set

    override var roll = 0f
        private set

    override fun onMeasure(measurement: Measurement) {
        addMeasurement(measurement)
        updateMeanAngles()
        updateCombinedAngles()
    }

    private fun addMeasurement(measurement: Measurement) {
        measurements.add(measurement)
        if (measurements.size > meanSampleSize) {
            measurements.removeIndex(0)
        }
    }

    private fun updateMeanAngles() {
        meanPitch = 0f
        meanRoll = 0f
        meanPitchYXZ = 0f
        meanRollYXZ = 0f
        for ((_, angles, anglesYXZ) in measurements) {
            meanPitch += angles.pitch / measurements.size
            meanRoll += angles.roll / measurements.size
            meanPitchYXZ += anglesYXZ.pitch / measurements.size
            meanRollYXZ += anglesYXZ.roll / measurements.size
        }
    }

    private fun updateCombinedAngles() {
        val proportion = abs(meanPitch) / (MathUtils.PI / 2f)
        val proportionYXZ = abs(meanPitchYXZ) / (MathUtils.PI / 2f)
        val weightedProportion = proportion * (1f - proportionYXZ)
        val weightedProportionYXZ = proportionYXZ * (1f - proportion)
        val totalWeightedProportion = weightedProportion + weightedProportionYXZ

        if (totalWeightedProportion > 0.000001f) {
            val norProportion = weightedProportion / totalWeightedProportion
            val norProportionYXZ = weightedProportionYXZ / totalWeightedProportion
            val combinedProportion = .5f * norProportion + .5f * (1f - norProportionYXZ)

            val minPitch = -MathUtils.PI / 2f
            val maxPitch = MathUtils.PI / 2f
            val pitchYXZRemappedBack = se.kcarlsson.vinkel.model.domain.remapPitchBackFromYXZ(meanPitchYXZ, meanRollYXZ)
            val pitchDiff = diffCyclic(meanPitch, pitchYXZRemappedBack, minPitch, maxPitch)
            pitch = norCyclic(meanPitch + combinedProportion * pitchDiff, minPitch, maxPitch)

            val minRoll = -MathUtils.PI
            val maxRoll = MathUtils.PI
            val rollYXZRemappedBack = se.kcarlsson.vinkel.model.domain.remapRollBackFromYXZ(meanPitchYXZ, meanRollYXZ)
            val rollDiff = diffCyclic(meanRoll, rollYXZRemappedBack, minRoll, maxRoll)
            roll = norCyclic(meanRoll + combinedProportion * rollDiff, minRoll, maxRoll)
        }
    }
}
