package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.math.Vector2
import se.kcarlsson.vinkel.util.GdxArray

interface ScreenSize {
    val width: Float
    val height: Float
    val max: Float
    val min: Float
    val minMaxDiff: Float

    val bottomLeft: Vector2
    val topLeft: Vector2
    val topRight: Vector2
    val bottomRight: Vector2
    val center: Vector2
    val corners: GdxArray<Vector2>

    fun isOnScreen(p: Vector2): Boolean =
        p.x in 0f..width && p.y in 0f..height
}