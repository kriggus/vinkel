package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.math.Intersector.intersectPolygons
import com.badlogic.gdx.math.Polygon

class Intersection(
    val polygon: Polygon = Polygon(FloatArray(128))
) {
    var isIntersecting: Boolean = false
        private set

    fun updateFromIntersection(p1: Polygon, p2: Polygon) {
        isIntersecting = intersectPolygons(p1, p2, polygon)
    }

    fun reset() {
        isIntersecting = false
    }
}