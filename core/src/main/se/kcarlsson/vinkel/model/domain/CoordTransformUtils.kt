package se.kcarlsson.vinkel.model.domain

import com.badlogic.gdx.math.MathUtils

fun remapPitchBackFromYXZ(pitch: Float, roll: Float): Float = when {
    pitch >= 0f && roll >= 0f -> -pitch + MathUtils.PI / 2
    pitch >= 0f && roll < 0f -> pitch - MathUtils.PI / 2
    pitch < 0f && roll >= 0f -> pitch + MathUtils.PI / 2
    // always pitch < 0f && roll < 0f
    else -> -pitch - MathUtils.PI / 2
}

fun remapRollBackFromYXZ(pitch: Float, roll: Float): Float = when {
    pitch >= 0f && roll >= 0f -> roll
    pitch >= 0f && roll < 0f -> -roll
    pitch < 0f && roll >= 0f -> -roll
    // always pitch < 0f && roll < 0f
    else -> roll
}
