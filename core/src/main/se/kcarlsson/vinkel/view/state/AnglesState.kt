package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.math.Intersector.intersectLines
import com.badlogic.gdx.math.MathUtils.*
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Disposable
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.app.AppTheme
import se.kcarlsson.vinkel.model.domain.ScreenEdge.*
import se.kcarlsson.vinkel.util.*
import se.kcarlsson.vinkel.view.render.ScreenSize
import se.kcarlsson.vinkel.view.render.Theme
import se.kcarlsson.vinkel.view.state.LockMode.LOCKED
import se.kcarlsson.vinkel.view.state.LockMode.STICKY
import kotlin.math.abs
import kotlin.math.roundToInt

class AnglesState(
    private val screen: ScreenSize = AppScreenSize.instance,
    theme: Theme = AppTheme.instance
) : Disposable {

    private var pitch = 0f
    private var roll = 0f
    private var lockedPitch = 0f
    private var lockedRoll = 0f
    private var isLocked = false
    private val up = Vector2()
    private val upTarget = Vector2()
    private var upScreenEdge = RIGHT
    private val screenEdgeIntersections = GdxArray(arrayOf(Vector2(), Vector2(), Vector2(), Vector2())) // left, top, right, bottom
    private val fontSize = screen.min / 16f
    private val fontLayout: GlyphLayout

    val overlayFrom = Vector2()
    val overlayFromCenter = Vector2()
    val overlayTo = Vector2()
    val overlayToCenter = Vector2()
    val overlayHeight: Float
    val overlayWidth: Float

    val font: BitmapFont
    val textTransform = Matrix4()
    var text = ""
        private set

    init {
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.size = fontSize.roundToInt()
        parameter.color = theme.foreground
        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))

        font = generator.generateFont(parameter)

        generator.dispose()

        fontLayout = GlyphLayout()
        fontLayout.setText(font, formatText(-90f, -90f))
        overlayWidth = fontLayout.width
        overlayHeight = 2f * fontLayout.height

        updateLayout()
    }

    fun updateVert(pitch: Float, roll: Float) {
        this.pitch = pitch
        this.roll = roll
        updateLayout()
        updateTextVert()
    }

    fun updateFlat(pitch: Float, roll: Float) {
        this.pitch = pitch
        this.roll = roll
        updateTextFlat()
    }

    fun updateLock(mode: LockMode) {
        isLocked = mode == LOCKED
        lockedPitch = when (mode) {
            STICKY -> pitch
            LOCKED -> lockedPitch
            else -> 0f
        }
        lockedRoll = when (mode) {
            STICKY -> roll
            LOCKED -> lockedRoll
            else -> 0f
        }
    }

    override fun dispose() {
        font.dispose()
    }

    private fun updateLayout() {
        updateTarget()
        interpolateToTarget()
        updateUpScreenEdge()
        updateOverlayPositions()
    }

    private fun updateTarget() {
        upTarget.x = -signPosZero(roll)
        upTarget.y = 0f
        upTarget.rotateRad(rotation())
        upTarget.add(screen.center)
    }

    private fun rotation(): Float =
        -signPosZero(roll) * pitch

    private fun interpolateToTarget() {
        up.x = lerp(up.x, upTarget.x, .33f)
        up.y = lerp(up.y, upTarget.y, .33f)
    }

    private fun updateUpScreenEdge() {
        intersectLines(screen.bottomLeft, screen.topLeft, screen.center, up, screenEdgeIntersections[0])
        intersectLines(screen.topLeft, screen.topRight, screen.center, up, screenEdgeIntersections[1])
        intersectLines(screen.topRight, screen.bottomRight, screen.center, up, screenEdgeIntersections[2])
        intersectLines(screen.bottomRight, screen.bottomLeft, screen.center, up, screenEdgeIntersections[3])

        var closestIntersection: Vector2? = null
        var minDst = Float.MAX_VALUE
        for (intersection in screenEdgeIntersections) {
            val dst = intersection?.dst2(up) ?: Float.MAX_VALUE
            if (dst < minDst) {
                minDst = dst
                closestIntersection = intersection
            }
        }

        upScreenEdge = when (closestIntersection) {
            screenEdgeIntersections[0] -> LEFT
            screenEdgeIntersections[1] -> TOP
            screenEdgeIntersections[2] -> RIGHT
            screenEdgeIntersections[3] -> BOTTOM
            else -> upScreenEdge
        }
    }

    private fun updateOverlayPositions() {
        val xMargin = screen.min / 16f + overlayHeight / 2f
        val yMargin = screen.min / 16f

        overlayFrom.x = when (upScreenEdge) {
            LEFT -> yMargin + overlayHeight
            TOP -> xMargin
            RIGHT -> screen.width - yMargin - overlayHeight
            BOTTOM -> screen.width - xMargin
        }
        overlayFrom.y = when (upScreenEdge) {
            LEFT -> xMargin
            TOP -> screen.height - yMargin - overlayHeight
            RIGHT -> screen.height - xMargin
            BOTTOM -> yMargin + overlayHeight
        }

        overlayFromCenter.x = when (upScreenEdge) {
            LEFT -> yMargin + overlayHeight / 2f
            TOP -> xMargin
            RIGHT -> screen.width - yMargin - overlayHeight / 2f
            BOTTOM -> screen.width - xMargin
        }
        overlayFromCenter.y = when (upScreenEdge) {
            LEFT -> xMargin
            TOP -> screen.height - yMargin - overlayHeight / 2f
            RIGHT -> screen.height - xMargin
            BOTTOM -> yMargin + overlayHeight / 2f
        }

        val rotation = when (upScreenEdge) {
            LEFT -> PI / 2f
            TOP -> 0f
            RIGHT -> -PI / 2f
            BOTTOM -> PI
        }
        overlayTo.x = overlayFrom.x + overlayWidth
        overlayTo.y = overlayFrom.y + overlayHeight
        overlayTo.rotateAroundRad(overlayFrom, rotation)

        overlayToCenter.x = overlayFromCenter.x + overlayWidth
        overlayToCenter.y = overlayFromCenter.y
        overlayToCenter.rotateAroundRad(overlayFromCenter, rotation)

        textTransform
            .idt()
            .translate(overlayFrom.x, overlayFrom.y, 0f)
            .rotateRad(0f, 0f, 1f, rotation)
            // Add fontLayout.height to compensate that font render text below overlayFrom.
            .translate(0f, fontLayout.height + (overlayHeight - fontLayout.height) / 2f, 0f)
    }

    private fun updateTextVert() {
        text = when (isLocked) {
            true -> {
                val xDiff = diffCyclic(xDegreesVert(lockedPitch, lockedRoll), xDegreesVert(pitch, roll), -180f, 180f)
                val yDiff = diffCyclic(yDegreesVert(lockedRoll), yDegreesVert(roll), -90f, 90f)
                formatText(xDiff, yDiff)
            }
            else ->
                formatText(xDegreesVert(pitch, roll), yDegreesVert(roll))
        }
    }

    private fun xDegreesVert(pitch: Float, roll: Float): Float {
        val remappedPitch = when {
            roll > 0f -> norCyclic(PI - pitch, -PI, PI)
            else -> pitch
        }
        return radiansToDegrees * remappedPitch
    }

    private fun yDegreesVert(roll: Float): Float {
        val remappedRoll = norCyclic(abs(roll) - PI / 2f, -PI / 2f, PI / 2f)
        return radiansToDegrees * remappedRoll
    }

    private fun updateTextFlat() {
        text = when (isLocked) {
            true -> {
                val xDiff = diffCyclic(degreesFlat(lockedPitch), degreesFlat(pitch), -90f, 90f)
                val yDiff = diffCyclic(degreesFlat(lockedRoll), degreesFlat(roll), -180f, 180f)
                formatText(xDiff, yDiff)
            }
            else ->
                formatText(degreesFlat(pitch), degreesFlat(roll))
        }
    }

    private fun degreesFlat(radians: Float): Float =
        radiansToDegrees * radians

    private fun formatText(xDegrees: Float, yDegrees: Float): String =
        "x: ${noDecimal.format(xDegrees)}° y: ${noDecimal.format(yDegrees)}°"
}
