package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.math.Polygon
import se.kcarlsson.vinkel.util.GdxArray

fun GdxArray<Intersection>.updateFromIntersection(polygon: Polygon, polygons: GdxArray<Polygon>) {
    populate(polygons.size)
    for (i in 0 until polygons.size) {
        this[i].updateFromIntersection(polygon, polygons[i])
    }
}

fun GdxArray<Intersection>.reset() {
    for (i in 0 until size) {
        this[i].reset()
    }
}

private fun GdxArray<Intersection>.populate(newSize: Int) {
    if (size != newSize) {
        setSize(newSize)
        for (i in 0 until size) {
            if (this[i] == null) {
                this[i] = Intersection()
            }
        }
    }
}