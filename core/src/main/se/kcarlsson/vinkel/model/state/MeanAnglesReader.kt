package se.kcarlsson.vinkel.model.state

import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement
import se.kcarlsson.vinkel.util.GdxArray

class MeanAnglesReader(
    private val meanSampleSize: Int
) : GyroscopeListener, AnglesHolder {

    private val measurements = GdxArray<Measurement>()

    override var pitch: Float = 0f
        private set

    override var roll: Float = 0f
        private set

    override fun onMeasure(measurement: Measurement) {
        addMeasurement(measurement)
        updateAngles()
    }

    private fun addMeasurement(measurement: Measurement) {
        measurements.add(measurement)
        if (measurements.size > meanSampleSize) {
            measurements.removeIndex(0)
        }
    }

    private fun updateAngles() {
        pitch = 0f
        roll = 0f
        for ((_, angles) in measurements) {
            pitch += angles.pitch / measurements.size
            roll += angles.roll / measurements.size
        }
    }
}
