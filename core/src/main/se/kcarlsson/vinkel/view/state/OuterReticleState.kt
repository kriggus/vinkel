package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.math.Intersector.intersectPolygons
import com.badlogic.gdx.math.Polygon
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.util.GdxArray
import se.kcarlsson.vinkel.util.rect
import se.kcarlsson.vinkel.view.render.ScreenSize

class OuterReticleState(
    screen: ScreenSize = AppScreenSize.instance,
    length: Float,
    thickness: Float
) {
    private val left = Polygon(FloatArray(8)).rect(
        originX = length / 2f,
        originY = screen.center.y,
        width = length,
        height = thickness
    )
    private val top = Polygon(FloatArray(8)).rect(
        originX = screen.center.x,
        originY = screen.height - (screen.minMaxDiff / 2f) - length / 2f,
        width = thickness,
        height = length
    )
    private val right = Polygon(FloatArray(8)).rect(
        originX = screen.width - length / 2f,
        originY = screen.center.y,
        width = length,
        height = thickness
    )
    private val bottom = Polygon(FloatArray(8)).rect(
        originX = screen.center.x,
        originY = screen.minMaxDiff / 2f + length / 2f,
        width = thickness,
        height = length
    )

    val rectile = GdxArray<Polygon>()

    init {
        rectile.add(left)
        rectile.add(top)
        rectile.add(right)
        rectile.add(bottom)
    }

    fun update(inner: GdxArray<Polygon>) {
        rectile.clear()
        addPartToRectileIfNotIntersecting(left, inner)
        addPartToRectileIfNotIntersecting(top, inner)
        addPartToRectileIfNotIntersecting(right, inner)
        addPartToRectileIfNotIntersecting(bottom, inner)
    }

    private fun addPartToRectileIfNotIntersecting(part: Polygon, polygons: GdxArray<Polygon>) {
        var isIntersection = false
        for (polygon in polygons) {
            isIntersection = isIntersection || intersectPolygons(polygon, part, null)
        }
        if (!isIntersection) {
            rectile.add(part)
        }
    }
}
