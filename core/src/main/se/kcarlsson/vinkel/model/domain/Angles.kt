package se.kcarlsson.vinkel.model.domain

data class Angles(
    val pitch: Float,
    val roll: Float
)
