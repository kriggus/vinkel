package se.kcarlsson.vinkel.model.state

import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement
import se.kcarlsson.vinkel.model.domain.Orientation
import se.kcarlsson.vinkel.util.GdxArray

class MeanOrientationReader(
    private val meanSampleSize: Int
) : GyroscopeListener, OrientationHolder {

    private val measurements = GdxArray<Measurement>()

    override var orientation: Orientation = Orientation.FLAT
        private set

    override fun onMeasure(measurement: Measurement) {
        addMeasurement(measurement)
        updateOrientation()
    }

    private fun addMeasurement(measurement: Measurement) {
        measurements.add(measurement)
        if (measurements.size > meanSampleSize) {
            measurements.removeIndex(0)
        }
    }

    private fun updateOrientation() {
        val last = measurements.lastOrNull()?.orientation ?: return
        var areAllSame = true
        for (measurement in measurements) {
            areAllSame = areAllSame && last == measurement.orientation
        }
        if (areAllSame && orientation != last) {
            orientation = last
        }
    }
}