package se.kcarlsson.vinkel.app

import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement

class GyroscopeLogger(
    private val app: Application = Gdx.app
) : GyroscopeListener {

    override fun onMeasure(measurement: Measurement) {
        app.debug(TAG, measurement.toString())
    }

    companion object {
        private const val TAG = "GyroscopeLogger"
    }
}
