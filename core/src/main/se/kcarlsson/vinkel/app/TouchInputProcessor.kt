package se.kcarlsson.vinkel.app

import com.badlogic.gdx.InputProcessor
import se.kcarlsson.vinkel.util.GdxArray

class TouchInputProcessor : InputProcessor {

    private val activePointers = mutableSetOf<Int>()
    private val listeners = GdxArray<TouchListener>()

    override fun touchCancelled(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean =
        false

    override fun keyDown(keycode: Int): Boolean =
        false

    override fun keyUp(keycode: Int): Boolean =
        false

    override fun keyTyped(character: Char): Boolean =
        false

    override fun touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean {
        val isFirst = activePointers.isEmpty()
        if (!activePointers.contains(pointer)) {
            activePointers.add(pointer)
        }
        if (isFirst) {
            for (listener in listeners) {
                listener.onTouchDown(x, y)
            }
        }
        return true
    }

    override fun touchUp(x: Int, y: Int, pointer: Int, button: Int): Boolean {
        activePointers.remove(pointer)
        if (activePointers.isEmpty()) {
            for (listener in listeners) {
                listener.onTouchUp(x, y)
            }
        }
        return true
    }

    override fun touchDragged(x: Int, y: Int, pointer: Int): Boolean =
        false

    override fun mouseMoved(x: Int, y: Int): Boolean =
        false

    override fun scrolled(amountX: Float, amountY: Float): Boolean =
        false

    fun addListener(listener: TouchListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: TouchListener) {
        listeners.removeValue(listener, true)
    }

    fun reset() {
        activePointers.clear()
    }
}
