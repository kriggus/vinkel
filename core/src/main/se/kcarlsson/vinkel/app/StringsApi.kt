package se.kcarlsson.vinkel.app

interface StringsApi {
    val appName: String
    val usageBtnText: String
    val usageDlgTitle: String
    val usageDlgMessage: String
}