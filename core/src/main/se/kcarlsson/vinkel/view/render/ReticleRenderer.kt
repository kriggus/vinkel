package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import se.kcarlsson.vinkel.app.AppTheme
import se.kcarlsson.vinkel.view.state.ReticleState

class ReticleRenderer(
    private val shapeRenderer: ShapeRenderer,
    private val theme: Theme = AppTheme.instance
) {
    fun render(state: ReticleState) {
        shapeRenderer.color = theme.foreground
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)

        for (part in state.rectile) {
            shapeRenderer.polygonFilled(part.vertices)
        }

        shapeRenderer.end()
    }
}
