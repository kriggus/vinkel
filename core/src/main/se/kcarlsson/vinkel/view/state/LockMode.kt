package se.kcarlsson.vinkel.view.state

enum class LockMode {
    DEFAULT,
    WAITING,
    STICKY,
    LOCKED
}
