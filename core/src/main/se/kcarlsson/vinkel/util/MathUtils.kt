package se.kcarlsson.vinkel.util

import kotlin.math.*

fun signPosZero(value: Float): Float =
    when (value) {
        0f -> 1f
        else -> sign(value)
    }

fun diffCyclic(x: Float, y: Float, min: Float, max: Float): Float {
    val small = if (x <= y) x else y
    val large = if (x <= y) y else x

    val diffLargeSmall = abs(large - small)
    val diffOverZero = abs(large) + abs(small)
    val diffOverMinMax = abs(min - small) + abs(max - large)

    val diffMin = selectSmallest(diffLargeSmall, diffOverZero, diffOverMinMax)

    val sign = when (diffMin) {
        diffOverMinMax -> -sign(y - x)
        else -> sign(y - x)
    }

    return sign * diffMin
}

private fun selectSmallest(vararg xs: Float): Float =
    xs.minOrNull()!!

fun norCyclic(n: Float, min: Float, max: Float): Float =
    when {
        n > max -> min + (n - max)
        n < min -> max + (n - min)
        else -> n
    }

fun rotateClockwiseAroundX(x: Float, y: Float, radians: Float, originX: Float, originY: Float): Float =
    (x - originX) * cos(-radians) - (y - originY) * sin(-radians) + originX

fun rotateClockwiseAroundY(x: Float, y: Float, radians: Float, originX: Float, originY: Float): Float =
    (x - originX) * sin(-radians) + (y - originY) * cos(-radians) + originY

fun rotationBetween(x1: Float, y1: Float, x2: Float, y2: Float): Float =
    atan2(y2, x2) - atan2(y1, x1)

fun isInCircle(x: Float, y: Float, circleX: Float, circleY: Float, radius: Float): Boolean =
    (x - circleX).pow(2f) + (y - circleY).pow(2f) <= radius.pow(2f)

fun isInRect(x: Float, y: Float, rectFromX: Float, rectFromY: Float, rectToX: Float, rectToY: Float): Boolean =
    x in rectFromX..rectToX && y in rectFromY..rectToY
