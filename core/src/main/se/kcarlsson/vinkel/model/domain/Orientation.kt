package se.kcarlsson.vinkel.model.domain

enum class Orientation(val value: String) {
    FLAT("F"),
    LANDSCAPE("L"),
    PORTRAIT("P")
}
