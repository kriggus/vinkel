package se.kcarlsson.vinkel.model.state

import se.kcarlsson.vinkel.model.domain.Orientation

interface OrientationHolder {
    val orientation: Orientation
}