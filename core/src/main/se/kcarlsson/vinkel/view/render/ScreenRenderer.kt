package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.Disposable
import se.kcarlsson.vinkel.view.state.ScreenState

class ScreenRenderer(
    private val shapeRenderer: ShapeRenderer = ShapeRenderer(),
    private val spriteBatch: SpriteBatch = SpriteBatch(),
    private val background: BackgroundRenderer = BackgroundRenderer(),
    private val circles: CirclesRenderer = CirclesRenderer(shapeRenderer),
    private val sky: SkyRenderer = SkyRenderer(shapeRenderer),
    private val reticle: ReticleRenderer = ReticleRenderer(shapeRenderer),
    private val intersection: IntersectionRenderer = IntersectionRenderer(shapeRenderer),
    private val angles: AnglesRenderer = AnglesRenderer(shapeRenderer, spriteBatch),
    private val info: InfoRenderer = InfoRenderer(shapeRenderer, spriteBatch)
) : Disposable {

    fun render(state: ScreenState) {
        background.render()
        circles.render(state.circles)
        sky.render(state.sky)
        reticle.render(state.reticle)
        intersection.render(state.intersections)
        angles.render(state.angles)
        info.render(state.info)
    }

    override fun dispose() {
        shapeRenderer.dispose()
        spriteBatch.dispose()
    }
}
