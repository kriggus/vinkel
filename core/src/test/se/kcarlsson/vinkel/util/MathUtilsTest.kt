package se.kcarlsson.vinkel.util

import org.junit.Assert.assertEquals
import org.junit.Test

class MathUtilsTest {

    private val delta = .00001f

    @Test
    fun `diffCyclic in interval -10 to 10`() {
        // two on pos
        assertEquals(-2f, diffCyclic(8f, 6f, -10f, 10f), delta)
        assertEquals(2f, diffCyclic(6f, 8f, -10f, 10f), delta)

        // two on neg
        assertEquals(8.8f, diffCyclic(-9.9f, -1.1f, -10f, 10f), delta)
        assertEquals(-8.8f, diffCyclic(-1.1f, -9.9f, -10f, 10f), delta)

        // one on each, over 0
        assertEquals(-9f, diffCyclic(5f, -4f, -10f, 10f), delta)
        assertEquals(9f, diffCyclic(-4f, 5f, -10f, 10f), delta)

        // one on each, over min/max
        assertEquals(-2f, diffCyclic(-9f, 9f, -10f, 10f), delta)
        assertEquals(2f, diffCyclic(9f, -9f, -10f, 10f), delta)

        // on zero and neg
        assertEquals(-9f, diffCyclic(0f, -9f, -10f, 10f), delta)
        assertEquals(9f, diffCyclic(-9f, 0f, -10f, 10f), delta)

        // on zero and pos
        assertEquals(9f, diffCyclic(0f, 9f, -10f, 10f), delta)
        assertEquals(-9f, diffCyclic(9f, 0f, -10f, 10f), delta)

        // on min and pos
        assertEquals(-1f, diffCyclic(-10f, 9f, -10f, 10f), delta)
        assertEquals(1f, diffCyclic(9f, -10f, -10f, 10f), delta)

        // on min and neg
        assertEquals(2f, diffCyclic(-10f, -8f, -10f, 10f), delta)
        assertEquals(-2f, diffCyclic(-8f, -10f, -10f, 10f), delta)

        // on max and pos
        assertEquals(-1f, diffCyclic(10f, 9f, -10f, 10f), delta)
        assertEquals(1f, diffCyclic(9f, 10f, -10f, 10f), delta)

        // on max and neg
        assertEquals(1f, diffCyclic(10f, -9f, -10f, 10f), delta)
        assertEquals(-1f, diffCyclic(-9f, 10f, -10f, 10f), delta)

        // same number
        assertEquals(0f, diffCyclic(-9f, -9f, -10f, 10f), delta)
        assertEquals(0f, diffCyclic(9f, 9f, -10f, 10f), delta)
    }

    @Test
    fun `diffCyclic in interval 2 to 10`() {
        // 2 pos
        assertEquals(-2f, diffCyclic(8f, 6f, 2f, 10f), delta)
        assertEquals(2f, diffCyclic(6f, 8f, 2f, 10f), delta)

        // over min/max
        assertEquals(-2f, diffCyclic(3f, 9f, 2f, 10f), delta)
        assertEquals(2f, diffCyclic(9f, 3f, 2f, 10f), delta)

        // on min
        assertEquals(-1f, diffCyclic(2f, 9f, 2f, 10f), delta)
        assertEquals(1f, diffCyclic(9f, 2f, 2f, 10f), delta)

        // on max
        assertEquals(-1f, diffCyclic(3f, 10f, 2f, 10f), delta)
        assertEquals(1f, diffCyclic(10f, 3f, 2f, 10f), delta)

        // on min and max
        assertEquals(0f, diffCyclic(2f, 10f, 2f, 10f), delta)
        assertEquals(0f, diffCyclic(10f, 2f, 2f, 10f), delta)
    }

    @Test
    fun `norCyclic in interval -10 to 10`() {
        // in interval
        assertEquals(1f, norCyclic(1f, -10f, 10f), delta)

        // on min
        assertEquals(-10f, norCyclic(-10f, -10f, 10f), delta)

        // below min
        assertEquals(5f, norCyclic(-15f, -10f, 10f), delta)

        // on max
        assertEquals(10f, norCyclic(10f, -10f, 10f), delta)

        // above max
        assertEquals(-8f, norCyclic(12f, -10f, 10f), delta)
    }
}