package se.kcarlsson.vinkel.util

const val noDecimal = "%.0f"
const val oneDecimal = "%.1f"
const val twoDecimals = "%.2f"

fun formatWithSign(f: Float, format: String = oneDecimal): String =
    when {
        f <= 0f -> format.format(f)
        else -> "+${format.format(f)}"
    }