package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.app.AppTheme
import kotlin.math.abs
import kotlin.math.sign

class DebugLineRenderer(
    private val shapeRenderer: ShapeRenderer,
    private val screen: ScreenSize = AppScreenSize.instance,
    private val theme: Theme = AppTheme.instance,
    private var useOffset: Boolean = false
) {
    private val from = Vector2()
    private val to = Vector2()
    private val fromTarget = Vector2()
    private val toTarget = Vector2()

    fun render(pitch: Float, roll: Float) {
        prepare(pitch, roll)
        render()
    }

    private fun render() {
        Gdx.gl.glEnable(GL30.GL_BLEND)
        Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA)

        shapeRenderer.color = theme.foreground
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.circle(from.x, from.y, 33f)
        shapeRenderer.rectLine(from, to, 10f)
        shapeRenderer.end()

        Gdx.gl.glDisable(GL30.GL_BLEND)
    }

    private fun prepare(pitch: Float, roll: Float) {
        updateTarget(roll, rotation(pitch, roll), offset(roll))
        updatePosition()
    }

    private fun rotation(pitch: Float, roll: Float): Float =
        -sign(roll) * pitch

    private fun offset(roll: Float): Float =
        offsetSign(roll) * offsetProportion(roll) * screen.max

    private fun offsetSign(roll: Float): Float =
        sign(roll) * sign(abs(roll) - (MathUtils.PI / 2f))

    private fun offsetProportion(roll: Float) =
        abs(abs(roll) - (MathUtils.PI / 2f)) / (MathUtils.PI / 2f)

    private fun updateTarget(roll: Float, rotation: Float, offset: Float) {
        fromTarget.y = screen.center.y - sign(roll) * .4f * screen.min

        fromTarget.x = screen.center.x
        if (useOffset) {
            fromTarget.x += offset
        }
        fromTarget.rotateAroundRad(screen.center, rotation)

        toTarget.y = screen.center.y + sign(roll) * .4f * screen.min
        toTarget.x = screen.center.x
        if (useOffset) {
            toTarget.x += offset
        }
        toTarget.rotateAroundRad(screen.center, rotation)
    }

    private fun updatePosition() {
        from.x = fromTarget.x
        from.y = fromTarget.y
        to.x = toTarget.x
        to.y = toTarget.y
    }
}
