package se.kcarlsson.vinkel.model.state

interface AnglesHolder {
    val pitch: Float
    val roll: Float
}