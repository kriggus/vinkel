package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL30
import se.kcarlsson.vinkel.app.AppTheme

class BackgroundRenderer(
    private val theme: Theme = AppTheme.instance
) {
    fun render() {
        Gdx.gl.glClearColor(theme.background.r, theme.background.g, theme.background.b, theme.background.a)
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT)
    }
}
