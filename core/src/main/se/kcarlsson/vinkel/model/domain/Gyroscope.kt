package se.kcarlsson.vinkel.model.domain

import com.badlogic.gdx.Input
import com.badlogic.gdx.math.MathUtils
import se.kcarlsson.vinkel.app.CoordTransformApi
import se.kcarlsson.vinkel.util.GdxArray
import kotlin.math.abs
import kotlin.math.asin
import kotlin.math.atan2

class Gyroscope(
    val interval: Float = 1f,
    private val input: Input,
    private val coordTransform: CoordTransformApi
) {
    private val listeners: GdxArray<GyroscopeListener> = GdxArray()
    private var accumulatedDelta: Float = 0f

    private val rotationCache = FloatArray(9)
    private val rotationCacheYXZ = FloatArray(9)

    var isStarted: Boolean = false
        private set

    fun start() {
        isStarted = true
    }

    fun stop() {
        isStarted = false
    }

    fun addListener(listener: GyroscopeListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: GyroscopeListener): Boolean =
        listeners.removeValue(listener, true)

    fun update(delta: Float) {
        if (!isStarted) return
        incAccumulatedDelta(delta)
        if (shouldMeasureNow()) {
            decAccumulatedDelta()
            val measurement = measure()
            notifyMeasure(measurement)
        }
    }

    private fun incAccumulatedDelta(delta: Float) {
        accumulatedDelta += delta
    }

    private fun decAccumulatedDelta() {
        accumulatedDelta -= interval
    }

    private fun shouldMeasureNow(): Boolean =
        accumulatedDelta > interval

    private fun notifyMeasure(measurement: Measurement) {
        for (listener in listeners) {
            listener.onMeasure(measurement)
        }
    }

    private fun measure(): Measurement {
        updateCache()
        val orientation = calcOrientationFromCache()
        val angles = Angles(calcPitchFrom(rotationCache), calcRollFrom(rotationCache))
        val anglesYXZ = Angles(-calcPitchFrom(rotationCacheYXZ), calcRollFrom(rotationCacheYXZ))
        return Measurement(orientation, angles, anglesYXZ)
    }

    private fun updateCache() {
        input.getRotationMatrix(rotationCache)
        coordTransform.remapMatrixToYXZ(rotationCache, rotationCacheYXZ)
    }

    private fun calcPitchFrom(cache: FloatArray): Float =
        asin(cache[7])

    private fun calcRollFrom(cache: FloatArray): Float =
        atan2(-cache[6], cache[8])

    private fun calcOrientationFromCache(): Orientation {
        val pitch = calcPitchFrom(rotationCache)
        val roll = calcRollFrom(rotationCache)
        return when {
            abs(pitch) < MathUtils.PI / 4f && abs(roll) < MathUtils.PI / 4f -> Orientation.FLAT
            abs(pitch) > MathUtils.PI / 4f -> Orientation.PORTRAIT
            else -> Orientation.LANDSCAPE
        }
    }
}
