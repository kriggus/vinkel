package se.kcarlsson.vinkel.model.state

import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement
import se.kcarlsson.vinkel.model.domain.remapPitchBackFromYXZ
import se.kcarlsson.vinkel.model.domain.remapRollBackFromYXZ

class AnglesYXZReader : GyroscopeListener, AnglesHolder {

    override var pitch: Float = 0f
        private set

    override var roll: Float = 0f
        private set

    override fun onMeasure(measurement: Measurement) {
        pitch = remapPitchBackFromYXZ(measurement.anglesYXZ.pitch, measurement.anglesYXZ.roll)
        roll = remapRollBackFromYXZ(measurement.anglesYXZ.pitch, measurement.anglesYXZ.roll)
    }
}
