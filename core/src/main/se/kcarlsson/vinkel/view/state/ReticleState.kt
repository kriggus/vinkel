package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.math.Polygon
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.util.GdxArray
import se.kcarlsson.vinkel.view.render.ScreenSize
import se.kcarlsson.vinkel.view.state.LockMode.DEFAULT
import se.kcarlsson.vinkel.view.state.LockMode.STICKY

class ReticleState(
    private val screen: ScreenSize = AppScreenSize.instance,
    private val length: Float = screen.min / (6f * 2f),
    private val thickness: Float = screen.min / (6f * 24f),
    val inner: InnerReticleState = InnerReticleState(screen, length, thickness),
    val outer: OuterReticleState = OuterReticleState(screen, length, thickness)
) {
    val rectile = GdxArray<Polygon>()

    init {
        refresh()
    }

    fun update(lockMode: LockMode, circles: CirclesState, sky: SkyState) {
        when {
            lockMode == DEFAULT -> inner.withoutOffset()
            lockMode == STICKY && circles.isVisible -> inner.withOffset(circles)
            lockMode == STICKY && sky.isVisible -> inner.withOffset(sky)
        }
        outer.update(inner.rectile)
        refresh()
    }

    private fun refresh() {
        rectile.clear()
        rectile.addAll(inner.rectile)
        rectile.addAll(outer.rectile)
    }
}
