package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.math.Intersector.intersectLines
import com.badlogic.gdx.math.MathUtils.PI
import com.badlogic.gdx.math.MathUtils.lerp
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Disposable
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.app.AppTheme
import se.kcarlsson.vinkel.app.StringsApi
import se.kcarlsson.vinkel.model.domain.ScreenEdge.*
import se.kcarlsson.vinkel.util.GdxArray
import se.kcarlsson.vinkel.util.isInCircle
import se.kcarlsson.vinkel.util.isInRect
import se.kcarlsson.vinkel.util.signPosZero
import se.kcarlsson.vinkel.view.render.ScreenSize
import se.kcarlsson.vinkel.view.render.Theme
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

class InfoState(
    strings: StringsApi,
    theme: Theme = AppTheme.instance,
    private val screen: ScreenSize = AppScreenSize.instance
) : Disposable {

    private val up = Vector2()
    private val upTarget = Vector2()
    private var upScreenEdge = RIGHT
    private val screenEdgeIntersections = GdxArray(arrayOf(Vector2(), Vector2(), Vector2(), Vector2())) // left, top, right, bottom

    private val overlayMargin = screen.min / 16f
    private val overlayHeight: Float

    val overlayWidth: Float
    var overlayRadius: Float
        private set
    val overlayFrom = Vector2()
    val overlayFromCenter = Vector2()
    val overlayTo = Vector2()
    val overlayToCenter = Vector2()
    var overlayRotation: Float = 0f
        private set

    private val fontSize = screen.min / 16f
    private val fontLayout: GlyphLayout

    val font: BitmapFont
    val textTransform = Matrix4()
    val text = strings.usageBtnText

    init {
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.size = fontSize.roundToInt()
        parameter.color = theme.foreground
        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))

        font = generator.generateFont(parameter)

        generator.dispose()

        fontLayout = GlyphLayout()
        fontLayout.setText(font, text)
        overlayWidth = fontLayout.width
        overlayHeight = 2f * fontLayout.height
        overlayRadius = overlayHeight / 2f

        update(0f, 0f)
    }

    fun update(pitch: Float, roll: Float) {
        updateTarget(pitch, roll)
        interpolateToTarget()
        updateUpScreenEdge()
        updateOverlayPositions()
    }

    fun isIntersecting(x: Float, y: Float): Boolean {
        val minX = min(overlayFrom.x - overlayMargin, overlayTo.x - overlayMargin)
        val minY = min(overlayFrom.y - overlayMargin, overlayTo.y - overlayMargin)
        val maxX = max(overlayFrom.x + overlayMargin, overlayTo.x + overlayMargin)
        val maxY = max(overlayFrom.y + overlayMargin, overlayTo.y + overlayMargin)
        val isInRect = isInRect(x, y, minX, minY, maxX, maxY)
        val isInFromCircle = isInCircle(x, y, overlayFromCenter.x, overlayFromCenter.y, overlayRadius + overlayMargin)
        val isInToCircle = isInCircle(x, y, overlayToCenter.x, overlayToCenter.y, overlayRadius + overlayMargin)
        return isInRect || isInFromCircle || isInToCircle
    }

    override fun dispose() {
        font.dispose()
    }

    private fun updateTarget(pitch: Float, roll: Float) {
        upTarget.x = -signPosZero(roll)
        upTarget.y = 0f
        upTarget.rotateRad(rotation(pitch, roll))
        upTarget.add(screen.center)
    }

    private fun rotation(pitch: Float, roll: Float): Float =
        -signPosZero(roll) * pitch

    private fun interpolateToTarget() {
        up.x = lerp(up.x, upTarget.x, .33f)
        up.y = lerp(up.y, upTarget.y, .33f)
    }

    private fun updateUpScreenEdge() {
        intersectLines(screen.bottomLeft, screen.topLeft, screen.center, up, screenEdgeIntersections[0])
        intersectLines(screen.topLeft, screen.topRight, screen.center, up, screenEdgeIntersections[1])
        intersectLines(screen.topRight, screen.bottomRight, screen.center, up, screenEdgeIntersections[2])
        intersectLines(screen.bottomRight, screen.bottomLeft, screen.center, up, screenEdgeIntersections[3])

        var closestIntersection: Vector2? = null
        var minDst = Float.MAX_VALUE
        for (intersection in screenEdgeIntersections) {
            val dst = intersection?.dst2(up) ?: Float.MAX_VALUE
            if (dst < minDst) {
                minDst = dst
                closestIntersection = intersection
            }
        }

        upScreenEdge = when (closestIntersection) {
            screenEdgeIntersections[0] -> LEFT
            screenEdgeIntersections[1] -> TOP
            screenEdgeIntersections[2] -> RIGHT
            screenEdgeIntersections[3] -> BOTTOM
            else -> upScreenEdge
        }
    }

    private fun updateOverlayPositions() {
        val screenWidth = when (upScreenEdge) {
            TOP, BOTTOM -> screen.width
            LEFT, RIGHT -> screen.height
        }
        val xMargin = screenWidth - fontLayout.width - overlayMargin - overlayRadius
        val yMargin = overlayMargin

        overlayFrom.x = when (upScreenEdge) {
            LEFT -> yMargin + overlayHeight
            TOP -> xMargin
            RIGHT -> screen.width - yMargin - overlayHeight
            BOTTOM -> screen.width - xMargin
        }
        overlayFrom.y = when (upScreenEdge) {
            LEFT -> xMargin
            TOP -> screen.height - yMargin - overlayHeight
            RIGHT -> screen.height - xMargin
            BOTTOM -> yMargin + overlayHeight
        }

        overlayFromCenter.x = when (upScreenEdge) {
            LEFT -> yMargin + overlayRadius
            TOP -> xMargin
            RIGHT -> screen.width - yMargin - overlayRadius
            BOTTOM -> screen.width - xMargin
        }
        overlayFromCenter.y = when (upScreenEdge) {
            LEFT -> xMargin
            TOP -> screen.height - yMargin - overlayRadius
            RIGHT -> screen.height - xMargin
            BOTTOM -> yMargin + overlayRadius
        }

        overlayRotation = when (upScreenEdge) {
            LEFT -> PI / 2f
            TOP -> 0f
            RIGHT -> -PI / 2f
            BOTTOM -> PI
        }
        overlayTo.x = overlayFrom.x + overlayWidth
        overlayTo.y = overlayFrom.y + overlayHeight
        overlayTo.rotateAroundRad(overlayFrom, overlayRotation)

        overlayToCenter.x = overlayFromCenter.x + overlayWidth
        overlayToCenter.y = overlayFromCenter.y
        overlayToCenter.rotateAroundRad(overlayFromCenter, overlayRotation)

        textTransform
            .idt()
            .translate(overlayFrom.x, overlayFrom.y, 0f)
            .rotateRad(0f, 0f, 1f, overlayRotation)
            // Add fontLayout.height to compensate that font render text below overlayFrom.
            .translate(0f, fontLayout.height + (overlayHeight - fontLayout.height) / 2f, 0f)
    }
}
