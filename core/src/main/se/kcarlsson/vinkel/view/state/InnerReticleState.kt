package se.kcarlsson.vinkel.view.state

import com.badlogic.gdx.math.Polygon
import se.kcarlsson.vinkel.app.AppScreenSize
import se.kcarlsson.vinkel.util.GdxArray
import se.kcarlsson.vinkel.util.rect
import se.kcarlsson.vinkel.util.rotateClockwiseAround
import se.kcarlsson.vinkel.util.rotationBetween
import se.kcarlsson.vinkel.view.render.ScreenSize

class InnerReticleState(
    private val screen: ScreenSize = AppScreenSize.instance,
    private val length: Float,
    private val thickness: Float
) {
    private val cross1 = GdxArray<Polygon>(arrayOf(
        Polygon(FloatArray(8)), // horizontal
        Polygon(FloatArray(8))  // vertical
    ))
    private val cross2 = GdxArray<Polygon>(arrayOf(
        Polygon(FloatArray(8)), // horizontal
        Polygon(FloatArray(8))  // vertical
    ))
    val rectile = GdxArray<Polygon>()

    init {
        withoutOffset()
    }

    fun withOffset(state: CirclesState) {
        val rotation = rotationBetween(state.offset.x, state.offset.y, 1f, 0f)
        cross1[0]
            .rect(state.positions[0].x, state.positions[0].y, length, thickness)
            .rotateClockwiseAround(state.positions[0].x, state.positions[0].y, rotation)
        cross1[1]
            .rect(state.positions[0].x, state.positions[0].y, thickness, length)
            .rotateClockwiseAround(state.positions[0].x, state.positions[0].y, rotation)
        cross2[0]
            .rect(state.positions[1].x, state.positions[1].y, length, thickness)
            .rotateClockwiseAround(state.positions[1].x, state.positions[1].y, rotation)
        cross2[1]
            .rect(state.positions[1].x, state.positions[1].y, thickness, length)
            .rotateClockwiseAround(state.positions[1].x, state.positions[1].y, rotation)
        rectile.clear()
        rectile.addAll(cross1)
        rectile.addAll(cross2)
    }

    fun withOffset(state: SkyState) {
        rectile.clear()
        if (state.hasScreenEdgeIntersections) {
            val fromEdge = state.screenEdgeIntersections[0]
            val toEdge = state.screenEdgeIntersections[1]
            val originX = (toEdge.x + fromEdge.x) / 2f
            val originY = (toEdge.y + fromEdge.y) / 2f
            val horizonY = toEdge.y - fromEdge.y
            val horizonX = toEdge.x - fromEdge.x
            val rotation = rotationBetween(horizonX, horizonY, 1f, 0f)
            cross1[0]
                .rect(originX, originY, length, thickness)
                .rotateClockwiseAround(originX, originY, rotation)
            cross1[1]
                .rect(originX, originY, thickness, length)
                .rotateClockwiseAround(originX, originY, rotation)
            rectile.addAll(cross1)
        }
    }

    fun withoutOffset() {
        cross1[0].rect(
            originX = screen.center.x,
            originY = screen.center.y,
            width = length,
            height = thickness
        )
        cross1[1].rect(
            originX = screen.center.x,
            originY = screen.center.y,
            width = thickness,
            height = length
        )
        rectile.clear()
        rectile.addAll(cross1)
    }
}
