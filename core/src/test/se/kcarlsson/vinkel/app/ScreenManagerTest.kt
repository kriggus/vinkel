package se.kcarlsson.vinkel.app

import org.junit.Test

class ScreenManagerTest {

    @Test
    fun `Can create`() {
        ScreenManager(
            coordTransform = object : CoordTransformApi {
                override fun remapMatrixToYXZ(rotation: FloatArray, result: FloatArray): FloatArray = FloatArray(0)
            },
            strings = object : StringsApi {
                override val appName: String
                    get() = "appName"
                override val usageDlgTitle: String
                    get() = "usage dlg title"
                override val usageDlgMessage: String
                    get() = "usage dlg message"
                override val usageBtnText: String
                    get() = "usage btn text"
            },
            dialog = object : DialogApi {
                override fun show(title: String, message: String, rotation: Float) = Unit
                override fun hide() = Unit
            }
        )
    }
}
