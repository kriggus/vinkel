package se.kcarlsson.vinkel.model.state

import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement

class AnglesXYZReader : GyroscopeListener, AnglesHolder {

    override var pitch: Float = 0f
        private set

    override var roll: Float = 0f
        private set

    override fun onMeasure(measurement: Measurement) {
        pitch = measurement.anglesXYZ.pitch
        roll = measurement.anglesXYZ.roll
    }
}