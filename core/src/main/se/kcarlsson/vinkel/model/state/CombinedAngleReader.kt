package se.kcarlsson.vinkel.model.state

import com.badlogic.gdx.math.MathUtils
import se.kcarlsson.vinkel.model.domain.GyroscopeListener
import se.kcarlsson.vinkel.model.domain.Measurement
import se.kcarlsson.vinkel.model.domain.remapPitchBackFromYXZ
import se.kcarlsson.vinkel.model.domain.remapRollBackFromYXZ
import se.kcarlsson.vinkel.util.diffCyclic
import se.kcarlsson.vinkel.util.norCyclic
import kotlin.math.abs

class CombinedAngleReader() : GyroscopeListener, AnglesHolder {

    override var pitch: Float = 0f
        private set

    override var roll: Float = 0f
        private set

    override fun onMeasure(measurement: Measurement) {
        val pitch = measurement.anglesXYZ.pitch
        val roll = measurement.anglesXYZ.roll
        val pitchYXZ = measurement.anglesYXZ.pitch
        val rollYXZ = measurement.anglesYXZ.roll

        val proportion = abs(pitch) / (MathUtils.PI / 2f)
        val proportionYXZ = abs(pitchYXZ) / (MathUtils.PI / 2f)
        val weightedProportion = proportion * (1f - proportionYXZ)
        val weightedProportionYXZ = proportionYXZ * (1f - proportion)
        val totalWeightedProportion = weightedProportion + weightedProportionYXZ
        val norProportion = weightedProportion / totalWeightedProportion
        val norProportionYXZ = weightedProportionYXZ / totalWeightedProportion
        val combinedProportion = .5f * norProportion + .5f * (1f - norProportionYXZ)

        if (totalWeightedProportion > 0.000001f) {
            val minPitch = -MathUtils.PI / 2f
            val maxPitch = MathUtils.PI / 2f
            val pitchYXZRemappedBack = remapPitchBackFromYXZ(pitchYXZ, rollYXZ)
            val pitchDiff = diffCyclic(pitch, pitchYXZRemappedBack, minPitch, maxPitch)
            this.pitch = norCyclic(pitch + combinedProportion * pitchDiff, minPitch, maxPitch)

            val minRoll = -MathUtils.PI
            val maxRoll = MathUtils.PI
            val rollYXZRemappedBack = remapRollBackFromYXZ(pitchYXZ, rollYXZ)
            val rollDiff = diffCyclic(roll, rollYXZRemappedBack, minRoll, maxRoll)
            this.roll = norCyclic(roll + combinedProportion * rollDiff, minRoll, maxRoll)
        }
    }
}
