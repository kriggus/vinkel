package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import se.kcarlsson.vinkel.app.AppTheme
import se.kcarlsson.vinkel.view.state.SkyState

class SkyRenderer(
    private val shapeRenderer: ShapeRenderer,
    private val theme: Theme = AppTheme.instance
) {
    fun render(state: SkyState) {
        if (state.isVisible) {
            shapeRenderer.color = theme.foreground
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
            shapeRenderer.polygonFilled(state.sky.vertices)
            shapeRenderer.end()
        }
    }
}
