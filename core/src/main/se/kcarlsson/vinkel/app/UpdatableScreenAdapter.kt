package se.kcarlsson.vinkel.app

import com.badlogic.gdx.ScreenAdapter

abstract class UpdatableScreenAdapter : ScreenAdapter() {
    override fun render(delta: Float) {
        update(delta)
        render()
    }
    abstract fun update(delta: Float)
    abstract fun render()
}
