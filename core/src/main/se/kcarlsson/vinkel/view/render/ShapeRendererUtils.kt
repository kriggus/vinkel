package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2

fun ShapeRenderer.polygonFilled(vertices: FloatArray) {
    if (vertices.size < 6) {
        throw IllegalArgumentException("Polylines must contain at least 3 points.")
    }
    if (vertices.size % 2 != 0) {
        throw IllegalArgumentException("Polylines must have an even number of vertices.")
    }

    val refX = vertices[0]
    val refY = vertices[1]

    for (i in 4..vertices.lastIndex step 2) {
        val x2 = vertices[i - 2]
        val y2 = vertices[i - 1]
        val x3 = vertices[i]
        val y3 = vertices[i + 1]
        triangle(refX, refY, x2, y2, x3, y3)
    }
}

fun ShapeRenderer.rect(p1: Vector2, p2: Vector2) {
    val width = p2.x - p1.x
    val height = p2.y - p1.y
    rect(p1.x, p1.y, width, height)
}

fun ShapeRenderer.circle(p: Vector2, radius: Float) {
    circle(p.x, p.y, radius)
}