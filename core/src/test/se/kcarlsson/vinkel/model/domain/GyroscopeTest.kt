package se.kcarlsson.vinkel.model.domain

import org.junit.Assert.*
import org.junit.Test

class GyroscopeTest {

    @Test
    fun `Does set interval`() {
        val gyroscope = Gyroscope(2f, StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        assertEquals(2f, gyroscope.interval)
    }

    @Test
    fun `Can start`() {
        val gyroscope = Gyroscope(input = StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        gyroscope.start()
        assertTrue(gyroscope.isStarted)
    }

    @Test
    fun `Can start and stop`() {
        val g = Gyroscope(input = StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        g.start()
        g.stop()
        assertFalse(g.isStarted)
    }

    @Test
    fun `Can add listener`() {
        val gyroscope = Gyroscope(input = StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        gyroscope.addListener(object : GyroscopeListener {
            override fun onMeasure(measurement: Measurement) {
            }
        })
    }

    @Test
    fun `Can't remove non added listener`() {
        val gyroscope = Gyroscope(input = StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        val listener = object : GyroscopeListener {
            override fun onMeasure(measurement: Measurement) {
            }
        }
        assertFalse(gyroscope.removeListener(listener))
    }

    @Test
    fun `Can remove listener`() {
        val gyroscope = Gyroscope(input = StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        val listener = object : GyroscopeListener {
            override fun onMeasure(measurement: Measurement) {
            }
        }
        gyroscope.addListener(listener)
        assertTrue(gyroscope.removeListener(listener))
    }

    @Test
    fun `Does measure when interval is reached if started`() {
        val gyroscope = Gyroscope(1f, StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        var measuredRoll: Float? = null
        var measuredPitch: Float? = null
        gyroscope.addListener(object : GyroscopeListener {
            override fun onMeasure(measurement: Measurement) {
                val (_, angles) = measurement
                measuredRoll = angles.roll
                measuredPitch = angles.pitch
            }
        })
        gyroscope.start()
        gyroscope.update(1.1f)
        assertNotNull(measuredRoll)
        assertNotNull(measuredPitch)
    }

    @Test
    fun `Doesn't measure when interval isn't reached`() {
        val gyroscope = Gyroscope(1f, StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        var onMeasureCallCount = 0
        gyroscope.addListener(object : GyroscopeListener {
            override fun onMeasure(measurement: Measurement) {
                onMeasureCallCount++
            }
        })
        gyroscope.update(.9f)
        assertEquals(0, onMeasureCallCount)
    }

    @Test
    fun `Doesn't measure when interval is reached but not started`() {
        val gyroscope = Gyroscope(1f, StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        var onMeasureCallCount = 0
        gyroscope.addListener(object : GyroscopeListener {
            override fun onMeasure(measurement: Measurement) {
                onMeasureCallCount++
            }
        })
        gyroscope.update(.9f)
        assertEquals(0, onMeasureCallCount)
    }

    @Test
    fun `Doesn't measure when interval is reached but stopped`() {
        val gyroscope = Gyroscope(1f, StaticGyroscopeInputFake(), coordTransform = CopyCoordTransformFake())
        var onMeasureCallCount = 0
        gyroscope.addListener(object : GyroscopeListener {
            override fun onMeasure(measurement: Measurement) {
                onMeasureCallCount++
            }
        })
        gyroscope.start()
        gyroscope.stop()
        gyroscope.update(.9f)
        assertEquals(0, onMeasureCallCount)
    }
}

