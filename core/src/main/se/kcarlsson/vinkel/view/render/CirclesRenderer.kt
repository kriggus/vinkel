package se.kcarlsson.vinkel.view.render

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import se.kcarlsson.vinkel.app.AppTheme
import se.kcarlsson.vinkel.view.state.CirclesState

class CirclesRenderer(
    private val shapeRenderer: ShapeRenderer,
    private val theme: Theme = AppTheme.instance
) {
    fun render(state: CirclesState) {
        if (state.isVisible) {
            shapeRenderer.color = theme.foreground
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
            shapeRenderer.polygonFilled(state.circle1.vertices)
            shapeRenderer.polygonFilled(state.circle2.vertices)
            shapeRenderer.end()
        }
    }
}
