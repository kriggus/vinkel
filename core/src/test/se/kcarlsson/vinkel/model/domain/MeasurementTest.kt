package se.kcarlsson.vinkel.model.domain

import org.junit.Assert.assertNotNull
import org.junit.Test

class MeasurementTest {

    @Test
    fun `Can create`() {
        val m = Measurement(Orientation.LANDSCAPE, Angles(0f, 1f), Angles(1f, 0f))
        assertNotNull(m)
    }
}
