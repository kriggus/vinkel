package se.kcarlsson.vinkel.app

interface DialogApi {
    fun show(title: String, message: String, rotation: Float)
    fun hide()
}
