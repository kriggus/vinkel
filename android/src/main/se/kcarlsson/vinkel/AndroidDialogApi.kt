package se.kcarlsson.vinkel

import android.app.AlertDialog
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnPreDrawListener
import android.widget.FrameLayout
import com.badlogic.gdx.math.MathUtils.radiansToDegrees
import se.kcarlsson.vinkel.app.DialogApi
import kotlin.math.max

class AndroidDialogApi(
    private val context: Context
) : DialogApi {

    private var dialog: AlertDialog? = null

    override fun show(title: String, message: String, rotation: Float) {
        Handler(Looper.getMainLooper()).post {
            dialog?.dismiss()
            dialog = AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .create()
                .apply {
                    val decorView = getDecorView() ?: return@apply

                    // Note: App uses radians rotating counter-clockwise while android api uses degrees rotating clockwise.
                    decorView.rotation = radiansToDegrees * -rotation

                    val preDraw = object : OnPreDrawListener {
                        override fun onPreDraw(): Boolean {
                            applyDimsSwappedWorkaround()
                            removeTitleDivider()
                            decorView.viewTreeObserver?.removeOnPreDrawListener(this)
                            return false
                        }
                    }
                    decorView.viewTreeObserver?.addOnPreDrawListener(preDraw)
                }
            dialog?.show()
        }
    }

    override fun hide() {
        Handler(Looper.getMainLooper()).post {
            dialog?.dismiss()
            dialog = null
        }
    }

    private fun AlertDialog.getDecorView() =
        window?.decorView

    private fun AlertDialog.getRootView() =
        (window?.decorView as? ViewGroup?)?.getChildAt(0)

    private fun AlertDialog.applyDimsSwappedWorkaround() {
        val rootView = getRootView() ?: return
        val maxDim = max(rootView.height, rootView.width)
        rootView.layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.WRAP_CONTENT,
            FrameLayout.LayoutParams.WRAP_CONTENT,
            Gravity.CENTER
        )
        window?.setLayout(maxDim, maxDim)
    }

    private fun AlertDialog.removeTitleDivider() {
        val titleDividerId = context.resources.getIdentifier("titleDivider", "id", "android")
        val titleDivider = dialog?.findViewById<View>(titleDividerId)
        titleDivider?.visibility = View.GONE
    }
}
