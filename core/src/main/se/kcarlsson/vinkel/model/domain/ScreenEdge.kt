package se.kcarlsson.vinkel.model.domain

enum class ScreenEdge {
    LEFT, TOP, RIGHT, BOTTOM
}